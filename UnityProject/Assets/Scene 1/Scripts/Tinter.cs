﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tinter : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        //Set the material colour on each shape to a random colour
        GetComponentInChildren<Renderer>().material.color = new Color( UnityEngine.Random.Range( 0f, 1f ), UnityEngine.Random.Range( 0f, 1f ), UnityEngine.Random.Range( 0f, 1f ) );
    }
    //The fact that the shapes in scene 1 are called 'squircles' made my day
}
