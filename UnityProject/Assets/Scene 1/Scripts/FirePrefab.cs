﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( Camera ) )]
public class FirePrefab : MonoBehaviour
{
    public GameObject Prefab;
    public float FireSpeed = 5;

    // Update is called once per frame
    void Update()
    {
        //If the fire button is pressed (mouse), 
        //Set the location the mouse clicked to a vector 3
        //Set direction of fire to the vector direction between the point clicked and the camera's position (normalized)
        //Instantiate the bullet/ball prefab at the position of the camera
        //Set the bullet/ball's velocity to the above established fire direction at a rate of fire speed
        if ( Input.GetButton( "Fire1" ) )
        {
            Vector3 clickPoint = GetComponent<Camera>().ScreenToWorldPoint( Input.mousePosition + Vector3.forward );
            Vector3 FireDirection = clickPoint - this.transform.position;
            FireDirection.Normalize();
            GameObject prefabInstance = GameObject.Instantiate( Prefab, this.transform.position, Quaternion.identity, null );
            prefabInstance.GetComponent<Rigidbody>().velocity = FireDirection * FireSpeed;
        }
    }
}
