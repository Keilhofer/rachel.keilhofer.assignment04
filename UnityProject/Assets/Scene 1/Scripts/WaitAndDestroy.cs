﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitAndDestroy : MonoBehaviour
{
    private IEnumerator destroyBall;

    void Awake()
    {
        destroyBall = DestroyBall();
        StartCoroutine(destroyBall);
    }

    private IEnumerator DestroyBall()
    {
        yield return new WaitForSeconds(2f);
        Destroy(this.gameObject);
    }
}
