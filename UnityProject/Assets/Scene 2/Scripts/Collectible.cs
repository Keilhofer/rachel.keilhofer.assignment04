﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    public float Radius;

    public void Update()
    {
        //Array of all colliders that touch or are inside the given radius on the collectible
        //If the collision is with the player, destroy the collectible
        Collider[] collidingColliders = Physics.OverlapSphere( this.transform.position, Radius );
        for ( int colliderIndex = 0; colliderIndex < collidingColliders.Length; ++colliderIndex )
        {
            if ( collidingColliders[colliderIndex].tag == "Player" )
            {
                Destroy( this.gameObject );
            }
        }
    }

}
