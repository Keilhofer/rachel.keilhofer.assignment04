﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCollectible : MonoBehaviour
{
    public GameObject Collectible;

    private GameObject m_currentCollectible;

    // Update is called once per frame
    void Update()
    {
        //If no collectible currently exists in the scene, instantiate one at the position of one of the spawners (randomly picked)
        if ( m_currentCollectible == null )
        {
            m_currentCollectible = Instantiate( Collectible, this.transform.GetChild( UnityEngine.Random.Range( 0, this.transform.GetChildCount() ) ).position, Collectible.transform.rotation );
        }
    }
}
