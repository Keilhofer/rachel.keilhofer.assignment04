﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClickToSetNavTarget : MonoBehaviour
{
    
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Move();
        }
	}

    void Move()
    {
        //Raycast between mouse and camera used to set the player's nav mesh destination to move towards the mouse location on the screen
        Camera camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition), out hit, float.MaxValue, LayerMask.GetMask("Ground")))
        {
            GetComponent<NavMeshAgent>().destination = hit.point;
        }
    }
}
